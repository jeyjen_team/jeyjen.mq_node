﻿using jeyjen.mq.server;
using System.Collections.Generic;

namespace test_server.api
{
    [container]
    public class main
    {
        #region общие
        [optional("firstname", "Имя пользователя", typeof(object))]
        #endregion
        #region по группам
        [required("id", "Идентификатор", typeof(string), "*")]
        [required("login", "Логин пользователя", typeof(string), "**")]
        #endregion
        #region результат
        [result("users", "Список пользователей", typeof(List<int>))]
        #endregion
        [retrieve]
        public void retrieve(request req)
        {
            req.reject("ouch");
        }
        [execute()]
        public void execute(request req)
        {
            req.resolve("{\"m\":\"hello world\"}");
        }
    }
}
