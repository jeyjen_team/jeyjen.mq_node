﻿using jeyjen.mq.server;
using System;

namespace test_server
{
    class Program
    {
		
		
        static void Main(string[] args)
        {
            var s = new engine();
            s.start(args);
            Console.ReadLine();
            s.stop();
        }
    }
}
