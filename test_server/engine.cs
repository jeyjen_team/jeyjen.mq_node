﻿using jeyjen.mq.server;
using Newtonsoft.Json.Linq;
using System;

namespace test_server
{
    [service("test.activemq.server", "Отладочный сервис", "Сервис для отладки взаимодействия через шину данных")]
    class engine : mq_server
    {
        public override void unresolved_request(request request)
        {
              
        }

        protected override void on_before_running()
        {   
        }
        protected override void on_running()
        {
        }
        
        protected override void on_before_paused()
        {
        }
        protected override void on_paused()
        {
        }
        protected override void on_before_stopped()
        {
        }
        protected override void on_stopped()
        {
        }
    }
}
