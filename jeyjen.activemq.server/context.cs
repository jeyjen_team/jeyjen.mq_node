﻿using jeyjen.extension;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace jeyjen.mq_server
{
    public class context
    {
        private request_message _request;
        private responce_message _responce;
        public context(JObject data)
        {
            _request = new request_message(data);
            _responce = new responce_message();
        }
        public request_message request { get { return _request; } }
        public responce_message responce { get { return _responce; } }
        
        #region subclasses
        public class request_message
        {
            JObject _data;
            internal request_message(JObject data)
            {
                _data = data;
            }
            public JObject data
            {
                get
                {
                    return _data;
                }
            }
        }
        public class responce_message
        {
            TaskCompletionSource<JObject> _tcs;
            JObject _data;
            internal responce_message()
            {
                _tcs = new TaskCompletionSource<JObject>();
                _data = new JObject();
            }
            internal Task<JObject> task { get { return _tcs.Task; } }
            public void resolve()
            {
                _tcs.TrySetResult(_data);
            }
            public void reject(Exception e)
            {
                if (e.is_null())
                    throw new ArgumentNullException("не указан объект ошибки");
                _tcs.TrySetException(e);
            }
        } 
        #endregion
    }
    
}
