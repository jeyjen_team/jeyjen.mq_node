﻿using Apache.NMS;
using Apache.NMS.Util;
using jeyjen.extension;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace jeyjen.mq_server
{
    public enum state
    {
        none,
        before_running,
        running,
        before_paused,
        paused,
        before_stopped,
        stopped
    }
    public abstract class mq_server
    {
        Dictionary<string, Tuple<object, MethodInfo>> _handler;

        string _performer;
        string _prefix;
        string _id;
        string _url;
        string _queue_name;
        string _queue_name_info;
        IConnectionFactory _factory;
        ISession _main_session = null;

        state _state;
        ReadOnlyDictionary<string, string> _config;
        
        public string queue
        {
            get
            {
                return _queue_name;
            }
        }
        public string queue_info
        {
            get
            {
                return _queue_name_info;
            }
        }
        public string url
        {
            get
            {
                return _url;
            }
        }
        public mq_server(params string[] args)
        {
            _state = state.none;
            
            var v = GetType().GetCustomAttribute<service>(false);
            if (v.is_null())
            {
                Console.WriteLine("does not defined service attribute");
            }
            _performer = v.name;
            define_handlers();
            
            var config = new Dictionary<string, string>();
            for (int i = 0; i < args.Length; i += 2)
            {
                config.Add(args[i].ToLower().Substring(1), args[i + 1]);
            }
            _config = new ReadOnlyDictionary<string, string>(config);
            init();

            Console.WriteLine("service {0} connected to {1}", _performer, _url);
            Console.WriteLine("listen queues: {0}", _queue_name);
            Console.WriteLine("instance id: {0}", _id);
            Task.Run(() =>
            {
                main_cycle();
            });
        }
        private void define_handlers()
        {
            _handler = new Dictionary<string, Tuple<object, MethodInfo>>();
            var ass = Assembly.GetEntryAssembly();
            foreach (var type in ass.GetTypes())
            {
                string container_name = null;
                var ctx_attr = type.GetCustomAttribute<container>(false);
                if (ctx_attr.is_null())
                    continue;

                object cont = null;
                try
                {
                    cont = Activator.CreateInstance(type);
                }
                catch (Exception e)
                {
                    throw new Exception("Ошибка при создании класса \"{0}\"".format(type.FullName));
                }

                container_name = ctx_attr.name.ToLower();
                foreach (var method in type.GetMethods())
                {
                    var method_attr = method.GetCustomAttribute<action>(false);
                    if (method_attr.is_null())
                        continue;
                    
                    string t = null;
                    if (method_attr is retrieve)
                        t = "retrieve";
                    else if( method_attr is execute)
                        t = "execute";
                    
                    var a = method_attr.name.ToLower();
                    
                    var pars = method.GetParameters();
                    if (pars.Length != 1 || !pars[0].ParameterType.is_equals(typeof(context)))
                    {
                        throw new Exception("У обработчика \"{0}:{1}\" в контексте \"{2}\" некорректные аргументы".format(t, a, container_name));
                    }

                    var key = prepare_handler_name(container_name, t, a);

                    if (_handler.ContainsKey(key))
                        throw new Exception("Обработчик \"{0}:{1}\" в контексте \"{2}\" уже определен".format(t, a, container_name));

                    _handler.Add(key, new Tuple<object, MethodInfo>(cont, method));
                }
            }
        }
        private string prepare_handler_name(string container, string type, string action)
        {
            return "{0}#{1}#{2}".format(container.ToLower(), type.ToLower(), action.ToLower()); 
        }
        private void init()
        {
            _url = "";
            _prefix = "";
            _id = "";
            _queue_name = "";
            _queue_name_info = "";

            if (_config.ContainsKey("url"))
            {
                _url = _config["url"];
            }
            if (_config.ContainsKey("prefix"))
            {
                _prefix = _config["prefix"];
            }
            if (_config.ContainsKey("id"))
            {
                _id = _config["id"];
            }
            // определение очереди
            var qn = new StringBuilder();
            var qn_info = new StringBuilder();
            if (!_prefix.is_null_or_empty())
            {
                qn.AppendFormat("{0}.",_prefix);
                qn_info.AppendFormat("{0}.", _prefix);
            }
            if (!_performer.is_null_or_empty())
            {
                qn.AppendFormat("@{0}", _performer);
                qn_info.AppendFormat("@{0}.service", _performer);
            }
            if (!_id.is_null_or_empty())
            {
                qn.AppendFormat(".#{0}", _id);
                qn_info.AppendFormat(".#{0}", _id);
            }
            else
            {
                _id = Guid.NewGuid().ToString().ToLower();               
            }


            _queue_name = qn.ToString();
            _queue_name_info = qn_info.ToString();

            _factory = new NMSConnectionFactory(_url);
        }
        public virtual void unresolved_request(string container, string action_type, string action, context ctx)
        {
            ctx.responce.reject(new ArgumentException("обработчик на запрос \"{0}:{1}\" контекста \"{2}\" не определен"
                .format(action_type, action, container)));
        }
        public void main_cycle()
        {
            IConnection connection = null;
            IMessageConsumer consumer = null;
            while (_state != state.stopped)
            {
                // установка соединение
                bool connected = false;
                while (! connected)
                {
                    try
                    {
                        connection = _factory.CreateConnection();
                        connection.Start();
                        connected = true;
                        Console.WriteLine("connected");
                    }
                    catch
                    {
                        Thread.Sleep(1000);
                    }
                }
                try
                {
                    _main_session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
                    consumer = _main_session.CreateConsumer(SessionUtil.GetDestination(_main_session, _queue_name, DestinationType.Queue));

                    while (_state != state.stopped)
                    {
                        switch (_state)
                        {
                            case state.before_running:
                                {
                                    on_before_running();
                                    _state = state.running;
                                }
                                break;
                            case state.running:
                                {
                                    while (_state == state.running)
                                    {
                                        var m = consumer.Receive(TimeSpan.FromSeconds(2));
                                        if (!m.is_null())
                                        {
                                            try
                                            { handle_request(m); } catch {}
                                        }
                                    }
                                } break;
                            case state.before_paused:
                                {
                                    try { on_before_paused(); } catch { }
                                    _state = state.paused;
                                    try { on_paused(); } catch { }
                                } break;
                            case state.paused:
                                {
                                    Thread.Sleep(500);
                                }break;
                            case state.before_stopped:
                                {
                                    try { on_before_stopped(); } catch { };
                                    _state = state.stopped;
                                } break;
                            case state.stopped:
                                {
                                    try { on_stopped(); } catch { };
                                } break;
                            default: { Thread.Sleep(500); }break;
                        }
                    }
                    try { consumer.Close(); } catch { }
                    try { _main_session.Close(); } catch { }
                    try { connection.Close(); } catch { }
                }
                catch (Exception e)
                {
                    Console.WriteLine("error on init service");
                    Console.WriteLine(e.Message);
                }
            }
            try { consumer.Dispose(); } catch { }
            try { _main_session.Dispose(); } catch { }
            try { connection.Stop(); } catch { }
            Console.WriteLine("service {0} is stoped", _performer);
        }
        #region manage
        public void start() { _state = state.before_running; }
        public void pause() { _state = state.before_stopped; }
        public void stop() {  _state = state.stopped; } 
        #endregion
        #region hooks
        protected virtual void on_before_running()
        { }
        protected virtual void on_running()
        { }
        protected virtual void on_before_stopped()
        { }
        protected virtual void on_stopped()
        { }
        protected virtual void on_before_paused()
        { }
        protected virtual void on_paused()
        { } 
        #endregion
        private void handle_request(IMessage message)
        {
            var action_type = "retrieve";
            if (message.Properties.Contains("action-type"))
                action_type = message.Properties.GetString("action-type");

            var container = "";
            if (message.Properties.Contains("container"))
                container = message.Properties.GetString("container");

            var action = "";
            if (message.Properties.Contains("action"))
                action = message.Properties.GetString("action");

            JObject jo = null;
            if (message is IBytesMessage)
            {
                var data = ((IBytesMessage)message).Content;
                if (data.Length > 0)
                {
                    var ms = new MemoryStream();
                    using (var reader = new BsonDataReader(ms))
                    {
                        jo = JObject.Load(reader);
                    }
                }
                else
                {
                    jo = new JObject();
                }
            }
            else
            {
                var text = ((ITextMessage)message).Text;
                if (text.Length > 0)
                    jo = JObject.Parse(text);
                else
                    jo = new JObject();
            }
            
            var ctx = new context(jo);
            
            var reply_to = message.NMSReplyTo;
            if (reply_to.is_null())
            {
                if (message.Properties.Contains("reply-to"))
                {
                    reply_to = SessionUtil.GetDestination(_main_session, message.Properties.GetString("reply-to"), DestinationType.Queue);
                }
            }
            var producer = _main_session.CreateProducer(reply_to);
            producer.DeliveryMode = MsgDeliveryMode.NonPersistent;

            #region on success
            ctx.responce.task.ContinueWith((t) =>
                {
                    var result = t.Result;
                    IBytesMessage m = _main_session.CreateBytesMessage();
                    if (! result.is_null())
                    {

                        var ms = new MemoryStream();
                        var writer = new BsonDataWriter(ms);
                        try
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(writer, result);
                            m.WriteBytes(ms.ToArray());
                        }
                        catch {}
                        finally
                        {
                            ms.Close();
                            writer.Close();
                        }
                    }

                    m.NMSCorrelationID = message.NMSCorrelationID;
                    m.Properties.SetString("result", "success");
                    producer.Send(m);
                    producer.Close();
                }, TaskContinuationOptions.OnlyOnRanToCompletion);
            #endregion
            #region on error
            ctx.responce.task.ContinueWith((t) =>
                {
                    Exception ex = t.Exception.InnerException;
                    var main = new JObject();
                    var w = main;
                    do
                    {
                        w.Add("type", ex.GetType().ToString());
                        w.Add("message", ex.Message);
                        w.Add("stack_trace", ex.StackTrace);
                        w.Add("source", ex.Source);
                        var inner = new JObject();
                        w.Add("inner_exception", inner);
                        w = inner;
                        ex = ex.InnerException;
                    }
                    while (! ex.is_null());

                    var m = _main_session.CreateBytesMessage();
                    m.NMSCorrelationID = message.NMSCorrelationID;
                    m.Properties.SetString("result", "error");
                    m.Properties.SetString("text", ex.Message);
                    var res = main.ToString();
                    m.WriteBytes(Encoding.UTF8.GetBytes(main.ToString()));
                    producer.Send(m);
                    producer.Close();
                }, TaskContinuationOptions.OnlyOnFaulted); 
            #endregion
            execute(container, action, action_type, ctx);
        }
        public void execute(string container, string action, string action_type, context ctx)
        {
            var key = prepare_handler_name(container, action_type, action);
            Tuple<object, MethodInfo> a;
            try
            {
                if (_handler.TryGetValue(key, out a))
                {
                    a.Item2.Invoke(a.Item1, new object[] { ctx });
                }
                else
                {
                    unresolved_request(container, action_type, action, ctx);
                }
            }
            catch (Exception e)
            {
                ctx.responce.reject(new Exception("не обработанное исключение внутри обработчика \"{0}:{1}\" контейнера \"{2}\": \"{3}\"".format(action_type, action, container, e.Message), e));
            }
        }

        private void send(string destination_name, byte[] data, string content_type)
        {
            var destination = SessionUtil.GetDestination(_main_session, destination_name, DestinationType.Queue);
            var producer = _main_session.CreateProducer(destination);
            producer.DeliveryMode = MsgDeliveryMode.NonPersistent;
            IBytesMessage m = _main_session.CreateBytesMessage();
            m.Properties.SetString("producer", _performer);
            m.WriteBytes(data);
            producer.Send(m);
            producer.Close();
        }
    }
}
