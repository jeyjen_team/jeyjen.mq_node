﻿using jeyjen.extension;
using System;

namespace jeyjen.mq_server
{
    #region service
    [AttributeUsage(AttributeTargets.Class)]
    public class service : Attribute
    {
        private string _name;
        private string _title;
        private string _description;
        public string name
        {
            get
            {
                return _name.is_null() ? "" : _name;
            }
        }
        public string description
        {
            get
            {
                return _description.is_null() ? "" : _description;
            }
        }
        public string title
        {
            get
            {
                return _title.is_null() ? "" : _title;
            }
        }
        public service(string name, string title, string description)
        {
            _name = name;
            _description = description;
            _title = title;
        }
    }

    #endregion
    #region request context
    [AttributeUsage(AttributeTargets.Class)]
    public class container : Attribute
    {
        private string _name;
        private string _description;
        public string name
        {
            get
            {
                return _name;
            }
        }
        public string description
        {
            get
            {
                return _description;
            }
        }

        public container(string name = "", string description = "")
        {
            _name = name;
            _description = description;
        }
    }
    #endregion
    #region request type
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class action : Attribute
    {
        private string _name;
        private string _description;
        public string name
        {
            get
            {
                return _name;
            }
        }
        public string description
        {
            get
            {
                return _description;
            }
        }
        public action(string name = "", string description = "")
        {
            _name = name;
            _description = description;
        }
    }
    [AttributeUsage(AttributeTargets.Method)]
    public class retrieve : action
    {
        public retrieve(string name = "", string description = "")
            : base(name, description) { }
    }
    [AttributeUsage(AttributeTargets.Method)]
    public class execute : action
    {
        public execute(string name = "", string description = "")
            : base(name, description) { }
    }
    [AttributeUsage(AttributeTargets.Method)]

    #endregion
    /// <summary>
    /// http://swagger.io/specification/
    /// </summary>
    
    #region arguments
    public abstract class option : Attribute
    {
        string _name;
        string _description;
        string _format;
        string _group;
        public option(string name, string description, Type type, string group = "", string format = "")
        {
            _name = name;
            _description = description;
            _format = format;
            _group = group;
        }
        public string name { get { return _name; } }
        public string description { get { return _description; } }
        public string format { get { return _format; } }
        public string group
        {
            get { return _group; }
        }

    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class required : option
    {
        public required(string name, string description, Type type, string group = "", string format = "")
            : base(name, description, type, group, format)
        { }
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class optional : option
    {
        public optional(string name, string description, Type type, string group = "", string format = "")
            : base(name, description, type, group, format)
        { }
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class result : Attribute
    {
        string _name;
        string _description;
        string _format;
        public result(string name, string description, Type type, string format = "")
        {
            _name = name;
            _description = description;
            _format = format;
        }
        public string name { get { return _name; } }
        public string description { get { return _description; } }
        public string format { get { return _format; } }
    } 
    #endregion

}
